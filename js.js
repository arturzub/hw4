class ArrayList {
    constructor(array) {
        if (Array.isArray(array)) {
            this.array = array;
        } else {
            this.array = [];
        }
    }

    push(...data) {
        for (let i = 0; i < data.length; i++) {
            this.array[this.array.length] = data[i];
        }
        return this.array.length;
    }

    pop() {
        if (this.array.length === 0) {
            return undefined;
        }
        let delItem = this.array[this.array.length - 1];
        this.array.length = this.array.length - 1;
        return delItem;
    }

    shift() {
        if (this.array.length === 0) {
            return undefined;
        }
        let delItem = this.array[0];
        for (let i = 1; i < this.array.length; i++) {
            this.array[i - 1] = this.array[i];
        }
        this.pop();

        return delItem;

    }

    unshift(...data) {
        for (let i = this.array.length - 1; i >= 0; i--) {
            this.array[i + data.length] = this.array[i];
            if (i < data.length) {
                this.array[i] = data[i];
            }
        }

        return this.array.length;

    }

    toString() {
        if (this.array.length === 0) {
            return '';
        }
        let string = '', arrLen = this.array.length - 1;

        this.array.forEach(function (item, i) {
            if (item === undefined || item === null) {
                item ='';
            }
            if (i === arrLen) {
                string += item
            } else {
                string += item + ',';
            }

        });
        return string;
    }

    sort() {
        const length = this.array.length;

        for (let i = length; i >= 0; i--) {

            for (let x = 0; x <= i - 1; x++) {

                if (this.array[x] > this.array[x + 1]) {
                    let tmp = this.array[x];
                    this.array[x] = this.array[x + 1];
                    this.array[x + 1] = tmp;
                }
            }
        }

        return this.array
    }
}

//let arr = [123, 333, -2, 56, 687, 8, -9];
let arr2 = [123, 333, -2, 56, 687, 8, -9];
arr = [null,null,undefined,NaN];
let lArray = new ArrayList(arr);

